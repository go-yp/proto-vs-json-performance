# Protobuf vs JSON performance

##### Structure
Golang:
```golang
type Ingredient struct {
	Code         uint32       `json:"code"`
	Name         string       `json:"name"`
	Value        uint32       `json:"value"`
	Require      bool         `json:"require"`
	Alternatives []Ingredient `json:"alternatives"`
}

type Cuisine struct {
	Code uint32 `json:"code"`
	Name string `json:"name"`
}

type DishCategory struct {
	Code uint32 `json:"code"`
	Name string `json:"name"`
}

type DishAuthor struct {
	FullName string `json:"full_name"`
}

// easyjson:json
type Recipe struct {
	ID             uint32         `json:"id"`
	Alias          string         `json:"alias"`
	Title          string         `json:"title"`
	Ingredients    []Ingredient   `json:"ingredients"`
	Cuisines       []Cuisine      `json:"cuisines"`
	DishCategories []DishCategory `json:"dish_categories"`
	ComplexLevel   uint32         `json:"complex_level"`
	TimeToComplete uint32         `json:"time_to_complete"`
	Rating         uint32         `json:"rating"`
	DishAuthors    []DishAuthor   `json:"dish_authors"`
}
```
Protobuf:
```proto
message Ingredient {
    uint32 code = 1;
    string name = 2;
    uint32 value = 3;
    bool require = 4;
    repeated Ingredient alternatives = 5;
}

message Cuisine {
    uint32 code = 1;
    string name = 2;
}

message DishCategory {
    uint32 code = 1;
    string name = 2;
}

message DishAuthor {
    string full_name = 1;
}

message Recipe {
    uint32 id = 1;
    string alias = 2;
    string title = 3;
    repeated Ingredient ingredients = 4;
    repeated Cuisine cuisines = 5;
    repeated DishCategory dish_categories = 6;
    uint32 complex_level = 7;
    uint32 time_to_complete = 8;
    uint32 rating = 9;
    repeated DishAuthor dish_authors = 10;
}
```

##### Result:

| Format                    | Bytes |
|---------------------------|-------|
| JSON with full names      | 502   |
| JSON with one letter name | 318   |
| Proto                     | 154   |

```text
BenchmarkFullMarshalJSONEmpty     	 2000000	       638 ns/op	     736 B/op	       4 allocs/op
BenchmarkFullUnmarshalJSONEmpty   	 2000000	       665 ns/op	       0 B/op	       0 allocs/op
BenchmarkFullMarshalJSON          	 1000000	      1713 ns/op	    1120 B/op	       5 allocs/op
BenchmarkFullUnmarshalJSON        	  500000	      3071 ns/op	     584 B/op	      15 allocs/op
BenchmarkMinMarshalJSONEmpty      	 5000000	       341 ns/op	     128 B/op	       1 allocs/op
BenchmarkMinUnmarshalJSONEmpty    	 2000000	       621 ns/op	       0 B/op	       0 allocs/op
BenchmarkMinMarshalJSON           	 1000000	      1546 ns/op	     896 B/op	       4 allocs/op
BenchmarkMinUnmarshalJSON         	  500000	      3068 ns/op	     584 B/op	      15 allocs/op
BenchmarkMarshalProtoEmpty        	 5000000	       313 ns/op	     208 B/op	       1 allocs/op
BenchmarkUnmarshalProtoEmpty      	 5000000	       300 ns/op	     400 B/op	       2 allocs/op
BenchmarkMarshalProto             	 1000000	      1225 ns/op	     696 B/op	       6 allocs/op
BenchmarkUnmarshalProto           	 1000000	      2047 ns/op	    1040 B/op	      22 allocs/op
```

##### Source:
```golang
var (
	fulljsonRecipe = &fulljson.Recipe{
		ID:    math.MaxUint32,
		Alias: "recipe-x-y-z",
		Title: "Recipe XYZ",
		Ingredients: []fulljson.Ingredient{
			{
				Code:    1,
				Name:    "Ingredient 1",
				Value:   2,
				Require: true,
				Alternatives: []fulljson.Ingredient{
					{
						Code:         1003,
						Name:         "Ingredient 1003",
						Value:        2,
						Require:      true,
						Alternatives: []fulljson.Ingredient{},
					},
				},
			},
			{
				Code:         2,
				Name:         "Ingredient 2",
				Value:        3,
				Require:      false,
				Alternatives: []fulljson.Ingredient{},
			},
		},
		Cuisines: []fulljson.Cuisine{
			{
				Code: 1,
				Name: "Cuisine 1",
			},
		},
		DishCategories: []fulljson.DishCategory{
			{
				Code: 1,
				Name: "Dish category 1",
			},
		},
		ComplexLevel:   1,
		TimeToComplete: 2,
		Rating:         3,
		DishAuthors: []fulljson.DishAuthor{
			{
				FullName: "Yaroslav",
			},
		},
	}
)
var (
	protoRecipe = &protos.Recipe{
		Id:    math.MaxUint32,
		Alias: "recipe-x-y-z",
		Title: "Recipe XYZ",
		Ingredients: []*protos.Ingredient{
			{
				Code:    1,
				Name:    "Ingredient 1",
				Value:   2,
				Require: true,
				Alternatives: []*protos.Ingredient{
					{
						Code:         1003,
						Name:         "Ingredient 1003",
						Value:        2,
						Require:      true,
						Alternatives: []*protos.Ingredient{},
					},
				},
			},
			{
				Code:         2,
				Name:         "Ingredient 2",
				Value:        3,
				Require:      false,
				Alternatives: []*protos.Ingredient{},
			},
		},
		Cuisines: []*protos.Cuisine{
			{
				Code: 1,
				Name: "Cuisine 1",
			},
		},
		DishCategories: []*protos.DishCategory{
			{
				Code: 1,
				Name: "Dish category 1",
			},
		},
		ComplexLevel:   1,
		TimeToComplete: 2,
		Rating:         3,
		DishAuthors: []*protos.DishAuthor{
			{
				FullName: "Yaroslav",
			},
		},
	}
)
```