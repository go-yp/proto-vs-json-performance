package benchmark

import (
	"github.com/micro/protobuf/proto"
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-yp/proto-vs-json-performance/models/protos"
	"math"
	"testing"
)

var (
	protoRecipe = &protos.Recipe{
		Id:    math.MaxUint32,
		Alias: "recipe-x-y-z",
		Title: "Recipe XYZ",
		Ingredients: []*protos.Ingredient{
			{
				Code:    1,
				Name:    "Ingredient 1",
				Value:   2,
				Require: true,
				Alternatives: []*protos.Ingredient{
					{
						Code:         1003,
						Name:         "Ingredient 1003",
						Value:        2,
						Require:      true,
						Alternatives: []*protos.Ingredient{},
					},
				},
			},
			{
				Code:         2,
				Name:         "Ingredient 2",
				Value:        3,
				Require:      false,
				Alternatives: []*protos.Ingredient{},
			},
		},
		Cuisines: []*protos.Cuisine{
			{
				Code: 1,
				Name: "Cuisine 1",
			},
		},
		DishCategories: []*protos.DishCategory{
			{
				Code: 1,
				Name: "Dish category 1",
			},
		},
		ComplexLevel:   1,
		TimeToComplete: 2,
		Rating:         3,
		DishAuthors: []*protos.DishAuthor{
			{
				FullName: "Yaroslav",
			},
		},
	}

	emptyProtoRecipeBytes = []byte{}
	protoRecipeBytes      = []byte("\x08\xff\xff\xff\xff\x0f\x12\x0crecipe-x-y-z\x1a\x0aRecipe XYZ\x22\x2e\x08\x01\x12\x0cIngredient 1\x18\x02 \x01\x2a\x18\x08\xeb\a\x12\x0fIngredient 1003\x18\x02 \x01\x22\x12\x08\x02\x12\x0cIngredient 2\x18\x03\x2a\x0d\x08\x01\x12\tCuisine 12\x13\x08\x01\x12\x0fDish category 1\x38\x01\x40\x02\x48\x03\x52\x0a\x0a\x08Yaroslav")
)

func TestMarshalProtoEmpty(t *testing.T) {
	recipe := &protos.Recipe{}
	content, err := proto.Marshal(recipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		emptyProtoRecipeBytes,
		content,
	)

	t.Logf("empty protobuf Recipe %d bytes", len(content))
}

func TestUnmarshalProtoEmpty(t *testing.T) {
	recipe := &protos.Recipe{}
	err := proto.Unmarshal(emptyProtoRecipeBytes, recipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		&protos.Recipe{},
		recipe,
	)
}

func TestMarshalProto(t *testing.T) {
	content, err := proto.Marshal(protoRecipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		protoRecipeBytes,
		content,
	)

	t.Logf("fill protobuf Recipe %d bytes", len(content))
}

func TestUnmarshalProto(t *testing.T) {
	recipe := &protos.Recipe{}
	err := proto.Unmarshal(protoRecipeBytes, recipe)

	if !assert.NoError(t, err) {
		return
	}

	assertEqualRecipe(
		t,
		protoRecipe,
		recipe,
	)
}

func BenchmarkMarshalProtoEmpty(b *testing.B) {
	recipe := &protos.Recipe{}

	for i := 0; i < b.N; i++ {
		_, _ = proto.Marshal(recipe)
	}
}

func BenchmarkUnmarshalProtoEmpty(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := &protos.Recipe{}

		_ = proto.Unmarshal(emptyProtoRecipeBytes, recipe)
	}
}

func BenchmarkMarshalProto(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = proto.Marshal(protoRecipe)
	}
}

func BenchmarkUnmarshalProto(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := &protos.Recipe{}

		_ = proto.Unmarshal(protoRecipeBytes, recipe)
	}
}

func assertEqualRecipe(t *testing.T, expect, actual *protos.Recipe) bool {
	t.Helper()

	if !assert.Equal(t, expect.Id, actual.Id) {
		return false
	}
	if !assert.Equal(t, expect.Alias, actual.Alias) {
		return false
	}
	if !assert.Equal(t, expect.Title, actual.Title) {
		return false
	}
	if !assert.Equal(t, expect.ComplexLevel, actual.ComplexLevel) {
		return false
	}
	if !assert.Equal(t, expect.TimeToComplete, actual.TimeToComplete) {
		return false
	}
	if !assert.Equal(t, expect.Rating, actual.Rating) {
		return false
	}

	if !assertEqualIngredients(t, expect.Ingredients, actual.Ingredients) {
		return false
	}
	if !assertEqualCuisines(t, expect.Cuisines, actual.Cuisines) {
		return false
	}
	if !assertEqualDishCategories(t, expect.DishCategories, actual.DishCategories) {
		return false
	}
	if !assertEqualDishAuthors(t, expect.DishAuthors, actual.DishAuthors) {
		return false
	}

	return true
}

func assertEqualIngredients(t *testing.T, expect, actual []*protos.Ingredient) bool {
	t.Helper()

	if !assert.Equal(t, len(expect), len(actual)) {
		return false
	}

	for i := range expect {
		if !assert.Equal(t, expect[i].Code, actual[i].Code) {
			return false
		}
		if !assert.Equal(t, expect[i].Name, actual[i].Name) {
			return false
		}
		if !assert.Equal(t, expect[i].Value, actual[i].Value) {
			return false
		}
		if !assert.Equal(t, expect[i].Require, actual[i].Require) {
			return false
		}

		if !assertEqualIngredients(t, expect[i].Alternatives, actual[i].Alternatives) {
			return false
		}
	}

	return true
}

func assertEqualCuisines(t *testing.T, expect, actual []*protos.Cuisine) bool {
	t.Helper()

	if !assert.Equal(t, len(expect), len(actual)) {
		return false
	}

	for i := range expect {
		if !assert.Equal(t, expect[i].Code, actual[i].Code) {
			return false
		}

		if !assert.Equal(t, expect[i].Name, actual[i].Name) {
			return false
		}
	}

	return true
}

func assertEqualDishCategories(t *testing.T, expect, actual []*protos.DishCategory) bool {
	t.Helper()

	if !assert.Equal(t, len(expect), len(actual)) {
		return false
	}

	for i := range expect {
		if !assert.Equal(t, expect[i].Code, actual[i].Code) {
			return false
		}

		if !assert.Equal(t, expect[i].Name, actual[i].Name) {
			return false
		}
	}

	return true
}

func assertEqualDishAuthors(t *testing.T, expect, actual []*protos.DishAuthor) bool {
	t.Helper()

	if !assert.Equal(t, len(expect), len(actual)) {
		return false
	}

	for i := range expect {
		if !assert.Equal(t, expect[i].FullName, actual[i].FullName) {
			return false
		}
	}

	return true
}
