package benchmark

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-yp/proto-vs-json-performance/models/fulljson"
	"math"
	"testing"
)

var (
	fullRecipe = fulljson.Recipe{
		ID:    math.MaxUint32,
		Alias: "recipe-x-y-z",
		Title: "Recipe XYZ",
		Ingredients: []fulljson.Ingredient{
			{
				Code:    1,
				Name:    "Ingredient 1",
				Value:   2,
				Require: true,
				Alternatives: []fulljson.Ingredient{
					{
						Code:         1003,
						Name:         "Ingredient 1003",
						Value:        2,
						Require:      true,
						Alternatives: []fulljson.Ingredient{},
					},
				},
			},
			{
				Code:         2,
				Name:         "Ingredient 2",
				Value:        3,
				Require:      false,
				Alternatives: []fulljson.Ingredient{},
			},
		},
		Cuisines: []fulljson.Cuisine{
			{
				Code: 1,
				Name: "Cuisine 1",
			},
		},
		DishCategories: []fulljson.DishCategory{
			{
				Code: 1,
				Name: "Dish category 1",
			},
		},
		ComplexLevel:   1,
		TimeToComplete: 2,
		Rating:         3,
		DishAuthors: []fulljson.DishAuthor{
			{
				FullName: "Yaroslav",
			},
		},
	}

	emptyJsonFullRecipe = []byte(`{"id":0,"alias":"","title":"","ingredients":null,"cuisines":null,"dish_categories":null,"complex_level":0,"time_to_complete":0,"rating":0,"dish_authors":null}`)
	jsonFullRecipe      = []byte(`{"id":4294967295,"alias":"recipe-x-y-z","title":"Recipe XYZ","ingredients":[{"code":1,"name":"Ingredient 1","value":2,"require":true,"alternatives":[{"code":1003,"name":"Ingredient 1003","value":2,"require":true,"alternatives":[]}]},{"code":2,"name":"Ingredient 2","value":3,"require":false,"alternatives":[]}],"cuisines":[{"code":1,"name":"Cuisine 1"}],"dish_categories":[{"code":1,"name":"Dish category 1"}],"complex_level":1,"time_to_complete":2,"rating":3,"dish_authors":[{"full_name":"Yaroslav"}]}`)
)

func TestFullMarshalJSONEmpty(t *testing.T) {
	recipe := &fulljson.Recipe{}
	content, err := recipe.MarshalJSON()

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		emptyJsonFullRecipe,
		content,
	)

	t.Logf("empty fulljson Recipe %d bytes", len(content))
}

func TestFullUnmarshalJSONEmpty(t *testing.T) {
	recipe := fulljson.Recipe{}
	err := recipe.UnmarshalJSON(emptyJsonFullRecipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		fulljson.Recipe{},
		recipe,
	)
}

func TestFullMarshalJSON(t *testing.T) {
	content, err := fullRecipe.MarshalJSON()

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		jsonFullRecipe,
		content,
	)

	t.Logf("fill fulljson Recipe %d bytes", len(content))
}

func TestFullUnmarshalJSON(t *testing.T) {
	recipe := fulljson.Recipe{}
	err := recipe.UnmarshalJSON(jsonFullRecipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		fullRecipe,
		recipe,
	)
}

func BenchmarkFullMarshalJSONEmpty(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := fulljson.Recipe{}

		_, _ = recipe.MarshalJSON()
	}
}

func BenchmarkFullUnmarshalJSONEmpty(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := fulljson.Recipe{}

		_ = recipe.UnmarshalJSON(emptyJsonFullRecipe)
	}
}

func BenchmarkFullMarshalJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = fullRecipe.MarshalJSON()
	}
}

func BenchmarkFullUnmarshalJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := fulljson.Recipe{}

		_ = recipe.UnmarshalJSON(jsonFullRecipe)
	}
}
