package benchmark

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/go-yp/proto-vs-json-performance/models/minjson"
	"math"
	"testing"
)

var (
	minRecipe = minjson.Recipe{
		ID:    math.MaxUint32,
		Alias: "recipe-x-y-z",
		Title: "Recipe XYZ",
		Ingredients: []minjson.Ingredient{
			{
				Code:    1,
				Name:    "Ingredient 1",
				Value:   2,
				Require: true,
				Alternatives: []minjson.Ingredient{
					{
						Code:         1003,
						Name:         "Ingredient 1003",
						Value:        2,
						Require:      true,
						Alternatives: []minjson.Ingredient{},
					},
				},
			},
			{
				Code:         2,
				Name:         "Ingredient 2",
				Value:        3,
				Require:      false,
				Alternatives: []minjson.Ingredient{},
			},
		},
		Cuisines: []minjson.Cuisine{
			{
				Code: 1,
				Name: "Cuisine 1",
			},
		},
		DishCategories: []minjson.DishCategory{
			{
				Code: 1,
				Name: "Dish category 1",
			},
		},
		ComplexLevel:   1,
		TimeToComplete: 2,
		Rating:         3,
		DishAuthors: []minjson.DishAuthor{
			{
				FullName: "Yaroslav",
			},
		},
	}

	emptyJsonMinRecipe = []byte(`{"i":0,"a":"","t":"","n":null,"c":null,"d":null,"l":0,"m":0,"r":0,"u":null}`)
	jsonMinRecipe      = []byte(`{"i":4294967295,"a":"recipe-x-y-z","t":"Recipe XYZ","n":[{"c":1,"n":"Ingredient 1","v":2,"r":true,"a":[{"c":1003,"n":"Ingredient 1003","v":2,"r":true,"a":[]}]},{"c":2,"n":"Ingredient 2","v":3,"r":false,"a":[]}],"c":[{"c":1,"n":"Cuisine 1"}],"d":[{"c":1,"n":"Dish category 1"}],"l":1,"m":2,"r":3,"u":[{"n":"Yaroslav"}]}`)
)

func TestMinMarshalJSONEmpty(t *testing.T) {
	recipe := &minjson.Recipe{}
	content, err := recipe.MarshalJSON()

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		emptyJsonMinRecipe,
		content,
	)

	t.Logf("empty minjson Recipe %d bytes", len(content))
}

func TestMinUnmarshalJSONEmpty(t *testing.T) {
	recipe := minjson.Recipe{}
	err := recipe.UnmarshalJSON(emptyJsonMinRecipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		minjson.Recipe{},
		recipe,
	)
}

func TestMinMarshalJSON(t *testing.T) {
	content, err := minRecipe.MarshalJSON()

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		jsonMinRecipe,
		content,
	)

	t.Logf("fill minjson Recipe %d bytes", len(content))
}

func TestMinUnmarshalJSON(t *testing.T) {
	recipe := minjson.Recipe{}
	err := recipe.UnmarshalJSON(jsonMinRecipe)

	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(
		t,
		minRecipe,
		recipe,
	)
}

func BenchmarkMinMarshalJSONEmpty(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := minjson.Recipe{}

		_, _ = recipe.MarshalJSON()
	}
}

func BenchmarkMinUnmarshalJSONEmpty(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := minjson.Recipe{}

		_ = recipe.UnmarshalJSON(emptyJsonMinRecipe)
	}
}

func BenchmarkMinMarshalJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = minRecipe.MarshalJSON()
	}
}

func BenchmarkMinUnmarshalJSON(b *testing.B) {
	for i := 0; i < b.N; i++ {
		recipe := minjson.Recipe{}

		_ = recipe.UnmarshalJSON(jsonMinRecipe)
	}
}
