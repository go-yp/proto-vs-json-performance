package minjson

type Ingredient struct {
	Code         uint32       `json:"c"`
	Name         string       `json:"n"`
	Value        uint32       `json:"v"`
	Require      bool         `json:"r"`
	Alternatives []Ingredient `json:"a"`
}

type Cuisine struct {
	Code uint32 `json:"c"`
	Name string `json:"n"`
}

type DishCategory struct {
	Code uint32 `json:"c"`
	Name string `json:"n"`
}

type DishAuthor struct {
	FullName string `json:"n"`
}

// easyjson:json
type Recipe struct {
	ID             uint32         `json:"i"`
	Alias          string         `json:"a"`
	Title          string         `json:"t"`
	Ingredients    []Ingredient   `json:"n"`
	Cuisines       []Cuisine      `json:"c"`
	DishCategories []DishCategory `json:"d"`
	ComplexLevel   uint32         `json:"l"`
	TimeToComplete uint32         `json:"m"`
	Rating         uint32         `json:"r"`
	DishAuthors    []DishAuthor   `json:"u"`
}
