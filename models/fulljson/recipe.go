package fulljson

type Ingredient struct {
	Code         uint32       `json:"code"`
	Name         string       `json:"name"`
	Value        uint32       `json:"value"`
	Require      bool         `json:"require"`
	Alternatives []Ingredient `json:"alternatives"`
}

type Cuisine struct {
	Code uint32 `json:"code"`
	Name string `json:"name"`
}

type DishCategory struct {
	Code uint32 `json:"code"`
	Name string `json:"name"`
}

type DishAuthor struct {
	FullName string `json:"full_name"`
}

// easyjson:json
type Recipe struct {
	ID             uint32         `json:"id"`
	Alias          string         `json:"alias"`
	Title          string         `json:"title"`
	Ingredients    []Ingredient   `json:"ingredients"`
	Cuisines       []Cuisine      `json:"cuisines"`
	DishCategories []DishCategory `json:"dish_categories"`
	ComplexLevel   uint32         `json:"complex_level"`
	TimeToComplete uint32         `json:"time_to_complete"`
	Rating         uint32         `json:"rating"`
	DishAuthors    []DishAuthor   `json:"dish_authors"`
}
