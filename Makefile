easyjson:
	rm -rf ./models/fulljson/*_easyjson.go
	easyjson ./models/fulljson
	rm -rf ./models/minjson/*_easyjson.go
	easyjson ./models/minjson

proto:
	protoc -I . protos/*.proto --go_out=plugins=grpc:models